-- chikun :: 2015
-- A tutorial covering the specific chikun LÖVE libraries



--[[
    Here we will load the chikun specific libaries
  ]]

love.graphics.setDefaultFilter('nearest')
love.graphics.setLineStyle('rough')
love.graphics.setLineWidth(0.25)

require "lib/bindings"      -- Very important that you read this file
require "lib/maths"         -- Load maths functions
require "lib/misc"          -- Load miscellaneous functions

--[[
    The recursive loading functions pretty much convert / to . and
    make file extensions void.
    eg. gfx/player.png becomes gfx.player,
        sfx/enemies/die becomes sfx.enemies.die
  ]]
require "lib/load/fonts"    -- Load fonts into fnt table [not recursive]
require "lib/load/gfx"      -- Load png graphics from gfx folder to gfx table
require "lib/load/maps"     -- Load all maps from map folder to maps table
require "lib/load/snd"      -- Load ogg sounds from bgm and sfx to bgm and sfx
require "lib/load/states"   -- Load states from src/states to states table

require "lib/checkInput"
require "lib/stateManager"  -- Load state manager
require "lib/mapTool"       -- Load mapTool :: don't try reading this

require "src/controls"
require "src/player"        -- Load player object and functions
require "src/scanlines"



-- Performed at game startup
function love.load()


    scanlines.create()

    -- Set current state to play state
    state.load(states.splC)

    heldKeys = { }

    pressedKeys = { }


end



-- Performed on game update
function love.update(dt)


    scanlines.update(dt)

    local wFactor, hFactor =
        g.getWidth()  / 200,
        g.getHeight() / 267

    local xOffset, yOffset = 0, 0

    if wFactor > hFactor then

        xOffset = g.getWidth() / 2 - 200 * hFactor / 2

    else

        yOffset = g.getHeight() / 2 - 267 * wFactor / 2

    end

    cursor = {
        x = ((m.getX() - xOffset) / wFactor) * math.max(wFactor / hFactor, 1),
        y = ((m.getY() - yOffset) / hFactor) * math.max(hFactor / wFactor, 1),
        w = 1, h = 1
    }


    --[[
        Limit dt to 1/15 and induce lag if below 15 FPS.
        This is to save calculations.
      ]]
    dt = math.min(dt, 1/15)


    -- Update current state
    state.current:update(dt)

    heldKeys = { }

    for key, value in pairs(pressedKeys) do

        heldKeys[key] = value

    end


end



-- Performed on game draw
function love.draw()


    -- Set drawing colour to white [default]
    g.setColor(255, 255, 255)


    rescale()


    -- Draw current state
    state.current:draw()

    scanlines.draw()
    -- Vignette
    g.setColor(255, 255, 255)
    g.draw(gfx.vignette, 0, 0)

    g.setScissor()

    g.origin()


end


function rescale()



    local wFactor, hFactor =
        g.getWidth()  / 200,
        g.getHeight() / 267

    local xOffset, yOffset = 0, 0

    if wFactor > hFactor then

        xOffset = g.getWidth() / 2 - 200 * hFactor / 2

    else

        yOffset = g.getHeight() / 2 - 267 * wFactor / 2

    end

    local scaleFactor = math.min(wFactor, hFactor)

    g.translate(xOffset, yOffset)

    g.setScissor(xOffset, yOffset, 200 * scaleFactor, 267 * scaleFactor)

    g.scale(scaleFactor)


end


function love.keypressed(key)

    pressedKeys[key] = true

end



function love.keyreleased(key)

    pressedKeys[key] = nil

end
