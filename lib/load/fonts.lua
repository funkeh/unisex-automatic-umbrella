-- chikun :: 2014
-- Loads all fonts from the /fnt folder


-- Can't do this recursively due to sizes
fnt = {

    -- Regular font
    regular = love.graphics.newFont("fnt/overdriveSunset.otf", 18),

    -- Smaller font
    smaller = love.graphics.newFont("fnt/overdriveSunset.otf", 14),

    options = love.graphics.newFont("fnt/onesr.ttf", 16),

    -- Splash screen font
    splash  = love.graphics.newFont("fnt/exo2.otf", 40),

    -- Score font
    score  = love.graphics.newFont("fnt/onesr.ttf", 12),

}
