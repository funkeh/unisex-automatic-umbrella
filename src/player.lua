-- chikun :: 2015
-- Player script



player = {}



-- Called when map loads
player.create = function()

    playerStates = {
        main = {
            image = gfx.andi.main,
            collisions = {
                {
                    x = 12,
                    y = 32,
                    w = 7,
                    h = 9
                },
                {
                    x = 15,
                    y = 39,
                    w = 5,
                    h = 10
                },
                {
                    x = 14,
                    y = 49,
                    w = 8,
                    h = 4
                },
                {
                    x = 13,
                    y = 51,
                    w = 3,
                    h = 14
                },
                {
                    x = 11,
                    y = 62,
                    w = 3,
                    h = 3
                },
                {
                    x = 9,
                    y = 65,
                    w = 3,
                    h = 9
                },
                {
                    x = 21,
                    y = 51,
                    w = 2,
                    h = 3
                },
                {
                    x = 22,
                    y = 54,
                    w = 3,
                    h = 17
                }
            },
            offset = {
                x = 17, y = 48
            }
        }
    }

    player.x = 100
    player.xSpeed = 0
    player.y = 64
    player.ySpeed = 0
    player.state = playerStates.main
    player.score = 0


end



-- Called using update phase
player.update = function(dt)


    player.xSpeed = player.xSpeed + getHorizontal() * dt * 1200

    if getHorizontal() == 0 then

        player.xSpeed = math.max(math.abs(player.xSpeed) - dt * 1200, 0) *
            math.sign(player.xSpeed)

    end

    player.xSpeed = math.clamp(-90, player.xSpeed, 90)


    player.ySpeed = player.ySpeed + getVertical() * dt * 1000

    if getVertical() == 0 then

        player.ySpeed = math.max(math.abs(player.ySpeed) - dt * 1000, 0) *
            math.sign(player.ySpeed)

    end

    player.ySpeed = math.clamp(-30, player.ySpeed, 50)

    -- Horizontal movement
    player.x = math.clamp(0, player.x + player.xSpeed * dt, 200)

    -- Vertical movement
    player.y = math.clamp(20, player.y + player.ySpeed * dt, 240)

    player.score = player.score + dt * 32

    for key, col in ipairs(player.state.collisions) do

        if math.overlapTable(items, {
                    x = col.x + player.x - 16,
                    y = col.y + player.y - 48,
                    w = col.w,
                    h = col.h
                }) then

            state.change(states.play)

        end

    end


end



-- Called using draw phase
player.draw = function()


    local scalFac = 40 / player.state.image:getWidth()

    g.draw(player.state.image, player.x, player.y, player.xSpeed / 1000, scalFac, scalFac,
        player.state.offset.x / scalFac, player.state.offset.y / scalFac)


end
