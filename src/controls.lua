deadzone = 0.2

function getHorizontal()

    local xMove = 0

    if input.isDown('left')  then   xMove = -1          end
    if input.isDown('right') then   xMove = xMove + 1   end

    if xMove == 0 then

        joy = j.getJoysticks()

        if joy[1] then

            xMove = joy[1]:getAxis(1)

            if math.abs(xMove) < deadzone then

                xMove = 0

            end
        end
    end

    return xMove

end


function getVertical()

    local yMove = 0

    if input.isDown('up')   then   yMove = -1          end
    if input.isDown('down') then   yMove = yMove + 1   end

    if yMove == 0 then

        joy = j.getJoysticks()

        if joy[1] then

            yMove = joy[1]:getAxis(2)

            if math.abs(yMove) < deadzone then

                yMove = 0

            end
        end
    end

    return yMove

end
