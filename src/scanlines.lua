lDown   = { }
lUp     = { }


scanlines = { }


function scanlines.create()

    for i=1, 267, 1 do

        lDown[i] = i

    end

    for i=1, 267, 1 do

       lUp[i] = i

    end

end



function scanlines.update(dt)


    for key, value in ipairs(lDown) do

        lDown[key] = (value + dt * 0.1) % 1002

    end

    for key, value in ipairs(lUp) do

        lUp[key] = value - dt * 0.2

        if lUp[key] < 0 then

            lUp[key] = lUp[key] + 1000

        end

    end


end


function scanlines.draw(alpha)

    local alpha = alpha or 8


    g.setColor(0, 0, 0, alpha)

    for key, value in ipairs(lDown) do

        g.line(0, value - 2.5, 200, value)

    end

    g.setColor(255, 255, 255, alpha)

    for key, value in ipairs(lUp) do

        g.line(0, value - 1.8, 200, value)

    end


end
