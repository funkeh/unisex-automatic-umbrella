-- chikun :: 2014
-- Splash state


-- Temporary state, removed at end of script
local splashState = { }


-- On state create
function splashState:create()


    -- Set background colour to black
    g.setBackgroundColor(0, 0, 0)

    -- Set off timer
    timer = 0

    -- Has sound played
    soundPlayed = false


end


-- On state update
function splashState:update(dt)


    -- Increase timer
    timer = timer + dt

    -- When two seconds passes, change state...
    if (timer > 2) then

        -- ...to the next state
        state.change(states.splO)

    -- If over half a second and sound hasn't played...
    elseif (timer >= 0.5 and not soundPlayed) then

        -- ...then play it!
        sfx.startup:play()

        -- And set variable
        soundPlayed = true

    end


end


-- On state draw
function splashState:draw()

    local splashAlpha = math.min(timer * 2, 1)

    if timer > 1.5 then

        splashAlpha = (2 - timer) * 2

    end

    -- Set colour with alpha
    g.setColor(255, 255, 255, splashAlpha * 255)

    -- Set font
    g.setFont(fnt.splash)

    -- Draw logo with formatting
    g.printf("chikun", 100, 133 - (fnt.splash:getHeight() / 2), 0, 'center')


end


-- On state kill
function splashState:kill()


    -- Kill all variables
    timer = nil
    soundPlayed = nil


end


-- Transfer data to state loading script
return splashState
