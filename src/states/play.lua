-- chikun :: 2014
-- Play state


-- Temporary state, removed at end of script
local playState = { }


-- On state create
function playState:create()

    player.create()

    bg = {
        gfx.bg.blue,
        gfx.bg.green,
        gfx.bg.orange,
        gfx.bg.purple,
    }

    items = { }

    eneDir = 0

    currentBG = 1

    inTimer = 0

    bgScroll = 64

    bgm.fallingDemo:play()

    shot = g.newImage(g.newScreenshot())

end


-- On state update
function playState:update(dt)

    inTimer = math.min(inTimer + dt * 4, 1)

    if inTimer == 1 then

        if math.random(1, 30) == 30 then

            table.insert(items, {
                    x = math.random(0, 190),
                    y = 300,
                    w = 10,
                    h = 8
                })

        end

        for key, value in ipairs(items) do

            value.y = value.y - dt * 80

            if value.y + value.h < 0 then

                table.remove(items, key)

            end

        end

        player.update(dt)

        eneDir = (eneDir + dt * 8) % 360

        bgScroll = bgScroll + (dt * 12)

        if bgScroll >= 233 then

            bgScroll = bgScroll - (233 - 64)

            currentBG = (currentBG % #bg) + 1

        end

    end

end


-- On state draw
function playState:draw()

    g.setColor(255, 255, 255)

    g.draw(bg[currentBG], -10, -bgScroll)

    if bgScroll > 169 then

        g.setColor(255, 255, 255, 255 * (bgScroll - 169) / 64)

        g.draw(bg[(currentBG % #bg) + 1], -10, 169-bgScroll)

    end

    for key, value in ipairs(items) do

        g.setColor(255, 255, 255)

        g.rectangle('fill', value.x, value.y, value.w, value.h)

    end

    g.setColor(255, 255, 255)

    player.draw()

    g.setColor(0, 0, 0, 64)

    g.rectangle('fill', 0, 0, 200, 24)

    g.setColor(0, 0, 0)

    g.draw(gfx.gui.fEnergy, 3, 3, 0, 20 / 94)

    g.setColor(255, 255, 255)

    g.draw(gfx.gui.fEnergy, 2, 2, 0, 20 / 94)

    g.setColor(0, 0, 0)

    g.draw(gfx.gui.fScore, 103, 3, 0, 20 / 94)

    g.setColor(255, 255, 255)

    g.draw(gfx.gui.fScore, 102, 2, 0, 20 / 94)

    g.setFont(fnt.score)

    g.setColor(0, 0, 0)

    g.print(math.floor(player.score), 151, 9, -0.1)

    g.setColor(255, 255, 255)

    g.print(math.floor(player.score), 150, 8, -0.1)

    if inTimer < 1 then

        g.setColor(255, 255, 255, (1 - inTimer) * 255)

        g.draw(shot, -200 * inTimer, 0, 0, 200 / shot:getWidth())

    end

end


-- On state kill
function playState:kill()

end


function playerCollides(obj)


    for key, box in ipairs(player.state.collisions) do

        if math.overlap(obj, {
                    x = player.x + box.x - player.state.offset.x,
                    y = player.y + box.y - player.state.offset.y,
                    w = box.w,
                    h = box.h
                }) then
            e.quit()
        end
    end
end


-- Transfer data to state loading script
return playState
