-- chikun :: 2014
-- Main menu state


-- Temporary state, removed at end of script
local mmState = { }


-- On state create
function mmState:create()

    mmVars = {
        fadeIn  = 1,
        hue     = 0,
        titleWave   = 20,
        umbrellas = { },
        optionsCanvas = g.newCanvas(200, 80),
        optionsWave = 20,
        optionsLoop = 0,
        optionsOut = 0,
        option = 1,
        options = {
            "START GAME",
            "HIGH SCORES",
            "OPTIONS",
            "EXIT GAME"
        },
        buttonMoved = false,
    }

    bgm.mainMenuTest:play()

end


-- On state update
function mmState:update(dt)

    -- Fade in
    mmVars.fadeIn = math.max(mmVars.fadeIn - dt * 2, 0)
    mmVars.hue = (mmVars.hue + (15 * dt)) % 255

    mmVars.optionsLoop = (mmVars.optionsLoop + dt * 260) % 360

    mmVars.optionsOut = (mmVars.optionsOut + dt * 1.4) % 3


    if mmVars.fadeIn == 0 then

        mmVars.titleWave = (mmVars.titleWave + dt * 18) % 360
        mmVars.optionsWave = (mmVars.optionsWave + dt * 15) % 360

        if math.random(1, 400) == 1 then

            table.insert(mmVars.umbrellas,
                {
                    x = math.random() * 200,
                    y = math.random() * 267,
                    s = 1
                }
            )

        end

        for key=#mmVars.umbrellas, 1, -1 do

            local value = mmVars.umbrellas[key]

            value.s = math.min(value.s + dt, 3)

            if value.s == 3 then

                table.remove(mmVars.umbrellas, key)

            end

        end

        -- Gather input, brah

        -- If pushed up this step
        if input.wasPressed("up") then

            mmVars.option = mmVars.option - 1

            if mmVars.option == 0 then

                mmVars.option = #mmVars.options

            end
        end

        -- If pushed down this step
        if input.wasPressed("down") then

            mmVars.option = (mmVars.option % #mmVars.options) + 1

        end

        -- If pushed action this step
        if input.wasPressed(" ") then

            if mmVars.option == 1 then

                bgm.mainMenuTest:stop()

                state.change(states.play)

            end
        end
    end
end


-- On state draw
function mmState:draw()

    g.setColor(HSL(mmVars.hue, 128, 128, 255))

    g.rectangle('fill', 0, 0, 200, 267)

    for key, value in ipairs(mmVars.umbrellas) do

        g.setColor(255, 255, 255, math.min(3 - value.s, 1) * 128)

        g.draw(gfx.umbrella, value.x, value.y, 0, value.s - 1, value.s - 1, 45, 45)

    end


    scanlines.draw(32)



    -- Options
    g.origin()
    g.setFont(fnt.options)
    g.setCanvas(mmVars.optionsCanvas)
    mmVars.optionsCanvas:clear()
    for key, value in ipairs(mmVars.options) do

        g.setColor(0, 0, 0, 128)
        g.printf(value, 1, 9 + (key - 1) * 16, 200, 'center')
        g.setColor(255, 255, 255)
        if key == mmVars.option then
            g.setColor(236, 255, 140)
        end
        g.printf(value, 0, 8 + (key - 1) * 16, 200, 'center')

        if key == mmVars.option then

            local co, si =
                 math.cos(mmVars.optionsLoop * math.pi / 180),
                math.sin(mmVars.optionsLoop * math.pi / 180)

            local variation = 0

            if mmVars.optionsOut > 2 then

                variation = 1 - (math.abs(mmVars.optionsOut - 2.5) * 2)

                    for i = 0, 16 do

                        g.setColor(255 - (19 * i / 16), 140 + (115 * i / 16),
                            211 - (71 * i / 16))

                        g.printf(value, 0, 8 + (key - 1) * 16 + co * 0.25 * variation * i,
                            200 + si * 0.25 * variation * i, 'center')

                    end

            end

            --g.setColor(255, 140, 211)

        end

    end
    g.setColor(255, 255, 255)
    g.setCanvas()
    rescale()
    g.draw(mmVars.optionsCanvas, 100, 164,
        math.sin(mmVars.optionsWave * math.pi / 180) * 0.12, 1, 1, 100, 40)



    -- Draw title
    for i=32, 0, -1 do

        local co, si, iVal =
            math.cos(mmVars.titleWave * 6 * math.pi / 180),
            math.sin(mmVars.titleWave * 6 * math.pi / 180),
            i / 32

        g.setFont(fnt.smaller)

        g.setColor(255 * (1 - iVal), 255 * (1 - iVal / 2), 255 * (1 - iVal / 2))

        if i == 1 then

            g.setColor(0, 0, 0)

            g.print("ANDI HISTERMINE'S", 101, 106,
                math.sin(mmVars.titleWave * math.pi / 180) * 0.10, 1, 1,
                g.getFont():getWidth("ANDI HISTERMINE'S") / 2 - 50,
                g.getFont():getHeight() / 2 + 20)

        else

            g.print("ANDI HISTERMINE'S", 100 + co * i / 12, 105 + si * i / 12,
                math.sin(mmVars.titleWave * math.pi / 180) * 0.10, 1, 1,
                g.getFont():getWidth("ANDI HISTERMINE'S") / 2 - 50,
                g.getFont():getHeight() / 2 + 20)

        end

        g.setFont(fnt.regular)

        g.setColor(250 * (iVal), 200 * (1 - iVal), 250)

        if i == 1 then

            g.setColor(0, 0, 0)

            g.print("UNISEX AUTOMATIC UMBRELLA", 101, 106,
                math.sin(mmVars.titleWave * math.pi / 180) * 0.15, 1, 1,
                g.getFont():getWidth("UNISEX AUTOMATIC UMBRELLA") / 2, g.getFont():getHeight() / 2)

        else

        g.print("UNISEX AUTOMATIC UMBRELLA", 100 + co * i / 12, 105 + si * i / 12,
            math.sin(mmVars.titleWave * math.pi / 180) * 0.15, 1, 1,
            g.getFont():getWidth("UNISEX AUTOMATIC UMBRELLA") / 2, g.getFont():getHeight() / 2)

        end

    end



    -- Fade in
    g.setColor(255, 255, 255, mmVars.fadeIn * 255)
    g.rectangle('fill', 0, 0, 750, 1000)

end


-- On state kill
function mmState:kill()

end


function HSL(h, s, l, a)
    if s<=0 then return l,l,l,a end
    h, s, l = h/256*6, s/255, l/255
    local c = (1-math.abs(2*l-1))*s
    local x = (1-math.abs(h%2-1))*c
    local m,r,g,b = (l-.5*c), 0,0,0
    if h < 1     then r,g,b = c,x,0
    elseif h < 2 then r,g,b = x,c,0
    elseif h < 3 then r,g,b = 0,c,x
    elseif h < 4 then r,g,b = 0,x,c
    elseif h < 5 then r,g,b = x,0,c
    else              r,g,b = c,0,x
    end return (r+m)*255,(g+m)*255,(b+m)*255,a
end


-- Transfer data to state loading script
return mmState
